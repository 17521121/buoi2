<?php 
  if(isset($_POST["userid"]))
    $userId = $_POST["userid"];
  if(isset($_POST["password"]))  
    $password = $_POST["password"];
  if(isset($_POST["password-confirmation"]))
    $passwordConfimation = $_POST["password-confirmation"];
  if(isset($_POST["age"]))
    $age = $_POST["age"];
 
  if($password != $passwordConfimation)
    echo "Password confimation must match password";
  else {
    try {
      $conn = new PDO('mysql:host=localhost; dbname=buoi2', 'admin','');
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare('INSERT INTO user VALUES (?,?,?)');
      $stmt->bindParam(1, $userId);
      $stmt->bindParam(2, $password);
      $stmt->bindParam(3, $age);
      $stmt->execute();
    
  
      header("Location: index.php?userid=".$userId);
      $conn = null;
      exit();   
    }
    catch(PDOException $e) {
      throw $e;
    }
 
  }
?>