<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <title>Homepage</title>
</head>
<body>
  <nav class="nav justify-content-center">
    <a class="nav-link active" href="./login.php">Login</a>
    <a class="nav-link" href="./signup.php">Sign up</a>
    <a class="nav-link disabled" href="#">
      <?php   
      if(isset($_GET['userid'])) {
        echo $_GET['userid'];
      } else 
        echo "Guess";
      ?>
    </a>
  </nav>
</body>
</html>